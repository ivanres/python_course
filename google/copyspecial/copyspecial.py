#!/usr/bin/python
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/

import sys
import re
import os
import shutil
import commands

"""Copy Special exercise
"""

# +++your code here+++
# Write functions and modify main() to call them
"""
Returns a list of the absolute paths of the special files in the given directory
"""
def get_special_paths(dir):
  res = []
  for filename in os.listdir(dir):
    if re.search(r'__\w+__', filename):
      res.append(os.path.abspath(os.path.join(dir, filename)))
  return res

def main():
  # This basic command line argument parsing code is provided.
  # Add code to call your functions below.

  # Make a list of command line arguments, omitting the [0] element
  # which is the script itself.
  args = sys.argv[1:]
  if not args:
    print "usage: [--todir dir][--tozip zipfile] dir [dir ...]";
    sys.exit(1)

  # todir and tozip are either set from command line
  # or left as the empty string.
  # The args array is left just containing the dirs.
  todir = ''
  if args[0] == '--todir':
    todir = args[1]
    del args[0:2]

  tozip = ''
  if args[0] == '--tozip':
    tozip = args[1]
    del args[0:2]

  if len(args) == 0:
    print "error: must specify one or more dirs"
    sys.exit(1)

  # +++your code here+++
  # Call your functions
  if todir:
    if not os.path.exists(todir):
      os.makedirs(todir)
    for dir in args:
      for path in get_special_paths(dir):
        shutil.copy(path, todir)
  elif tozip:
    cmd = 'zip -j %s' % tozip
    for dir in args:
      for path in get_special_paths(dir):
        cmd += ' ' + path
    print 'Command to run:', cmd
    (status, output) = commands.getstatusoutput(cmd)
    if status:
      sys.stderr.write(output)
      sys.exit(status)
  else:
    for dir in args:
      for path in get_special_paths(dir):
        print path
  
if __name__ == "__main__":
  main()
